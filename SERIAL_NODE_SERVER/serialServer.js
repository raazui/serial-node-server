var serialport = require("serialport"),		
SerialPort  = serialport.SerialPort,	   
express = require('express'),				   
open = require('open'),                   
url = 'http://localhost:8000';            

var app = express(),								   
server = require('http').createServer(app);		
io = require('socket.io').listen(server);		


var portName = 'COM3';				  

console.log("opening serial port: " + portName);	

server.listen(8000);								         
console.log((new Date()) + " SERIAL PORT COM Port "+ portName);
console.log((new Date()) + " SERIAL PORT SERVER Listening on " + url);

open(url);                   


var myPort = new SerialPort(portName, { 
    parser: serialport.parsers.readline("\r\n") 
});

app.get('/', function (request, response) {
    response.sendfile(__dirname + '/index.html');
});

app.get('/app.js', function (request, response) {
    response.sendfile(__dirname + '/app.js');
});

//app.get('/circuit/circuit.jar',function(request,response) {
//    response.sendfile(_dirname + '/circuit/circuit.jar');
//})

//smoothie.js

app.get('/smoothie.js', function (request, response) {
    response.sendfile(__dirname + '/smoothie.js');
});

var buffer = [];
io.sockets.on('connection', function (socket) {
	
	var address = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address;
	console.log((new Date()) + "NEW CONNECTION: LOCAL PROXY CLIENT @ " + address + ":" + socket.handshake.address.port);

    myPort.on('data', function (data) {
		var val = parseFloat(data); //12 bit raw count value 
		//val = val * (5/4095); //0 to 5v : 0 to 4095 counts
		buffer.push(val);
		// there are 2 sensors or series!!
		if(buffer.length == 2) {
		    socket.emit('serialEvent', buffer);
		    buffer = [];
		}
    }); //Serial data event end


    socket.on('disconnect',function (socket) {
			console.log((new Date()) + " LOCAL PROXY CLIENT Disconneted");
    });//Socket disconnection event end

});//Socket connection event end



