// open a connection to the serial server:
var socket_local = io.connect();//('http://localhost:8000');

var cloud_addr = "http://162.243.114.125:8080/my-stream2";

var socket_cloud = io.connect(cloud_addr);

var outputValue = false;		// value to send to the server

var chart = new SmoothieChart({millisPerPixel:200,grid:{sharpLines:true},labels:{disabled:true},maxValue:8,minValue:-1}),
    canvas = document.getElementById('smoothie-chart'),
    series = new TimeSeries(),
    series1 = new TimeSeries();

//Update client page
update_text("Now Streaming too: "+ cloud_addr + "\n", "message_box");

/*
var chart = new SmoothieChart({interpolation:'step', maxValue:5,minValue:0}),
    canvas = document.getElementById('smoothie-chart'),
    series = new TimeSeries(),
    series1 = new TimeSeries();
*/
chart.addTimeSeries(series, {lineWidth:2,strokeStyle:'#00ff00'});
chart.addTimeSeries(series1, {lineWidth:2,strokeStyle:'#fff000'});
//chart.addTimeSeries(series2, {lineWidth:2,strokeStyle:'#ff0000'});

chart.streamTo(canvas, 500);

// when you get a serialdata event, do this:
socket_local.on('serialEvent', function (data) {
//    console.log(data);
    var vals = data.map(function(val) { return (val / 1); });
    series.append(new Date().getTime(), vals[0]);//i need between 0..1
    series1.append(new Date().getTime(), vals[1]);//i need between 0..1   
    
    //Now Push serial data to the Cloud (remote server)
    //socket_cloud.send(JSON.stringify(data));
    socket_cloud.emit('serialEvent', data);
        
});

// when you get a message from cloud, do this:
socket_cloud.on('server_message', function (data) {
//    console.log("Cloud Server: " + data);
    //Update client page
    update_text(data + "\n", "message_box");
});

// when you get a message from cloud, do this:
socket_cloud.on('cloud_heart_beat', function (data) {
//    console.log("Cloud Server: " + data.date);
    //Update client page
    update_text(data.date + "\n", "message_box");
});

//Clear Button - On Click Event Handler
var clear_btn = document.getElementById('clear_btn');
//add event listener
clear_btn.addEventListener('click', function(event) {
  clear_text("message_box");
});

function update_text(text,box_id){
    var TheTextBox = document.getElementById(box_id);
    TheTextBox.value = TheTextBox.value + text;
}
function clear_text(box_id){
    var TheTextBox = document.getElementById(box_id);
    TheTextBox.value = "";
}
