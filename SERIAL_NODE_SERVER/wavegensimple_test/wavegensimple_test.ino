/*
  /*
  Simple Waveform generator with Arduino 
  
  TRANSMITS SAMPLES FOR TWO WAVEFORM SERIES SIMULTANEOUSLY
  THIS ONE JUST MODULATES ALL DIFFERENT WAVEFORM SHAPES WITH A SQUARE WAVE
   
  HARSH SINGH

 */

#include "Waveforms.h"
//#define maxSamplesnum 120
#define oneHzSample 1000000/maxSamplesNum  // sample for the 1Hz signal expressed in microseconds 

//const int button0 = 2, button1 = 3;
volatile int wave0 = 0, wave1 = 3;
int wave0_val = 0, wave1_val = 0;

int i = 0;
int sample;


void setup() {
  //analogWriteResolution(12);  // set the analog output resolution to 12 bit (4096 levels)
  //analogReadResolution(12);   // set the analog input resolution to 12 bit 

  //attachInterrupt(button0, wave0Select, RISING);  // Interrupt attached to the button connected to pin 2
  //attachInterrupt(button1, wave1Select, RISING);  // Interrupt attached to the button connected to pin 3

  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

void loop() {
  // Read the the potentiometer and map the value  between the maximum and the minimum sample available
  // 1 Hz is the minimum freq for the complete wave
  // 170 Hz is the maximum freq for the complete wave. Measured considering the loop and the analogRead() time
  //sample = map(analogRead(A0), 0, 4095, 0, oneHzSample);
  sample = 100;//constrain(t_sample, 0, oneHzSample);
  
  //analogWrite(DAC0, waveformsTable[wave0][i]);  // write the selected waveform on DAC0
  //analogWrite(DAC1, waveformsTable[wave1][i]);  // write the selected waveform on DAC1

  i++;
  if(i == maxSamplesNum){  // Reset the counter to repeat the wave
    i = 0;
    wave0++;  //Select next waveform type
    wave0 =  wave0 % maxWaveform; //Reset waveform type 
  }
  
  //Use the square pulse to control the other waveform output
  wave1_val = waveformsTable[wave1][i];
  if (wave1_val > 0){
      wave0_val = waveformsTable[wave0][i];
  }else{
      wave0_val = wave0_val + 409;
      wave0_val = wave0_val % 4095;
  }

  delayMicroseconds(sample);  // Hold the sample value for the sample time

  Serial.println(wave0_val, DEC);  //Send Sample value to serial for waveform1 
  Serial.println(wave1_val, DEC);  //Send Sample value to serial for waveform2

  delay(100);        // delay in between reads for stability
}

